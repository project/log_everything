<?php

namespace Drupal\Tests\log_everything\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test content CRUD operation will generate logs.
 *
 * @group log_everything
 */
class ContentEntityLogTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dblog', 'log_everything', 'user'];

  /**
   * A regular user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->webUser = $this->drupalCreateUser([], NULL, TRUE);
  }

  /**
   * Test that a content CRUD will register a log operation.
   */
  public function testContentEntityLog() {
    $this->drupalLogin($this->webUser);

    $this->drupalGet('/admin/people/create');
    $this->getSession()->getPage()->fillField('Username', 'Test');
    $this->getSession()->getPage()->fillField('Password', 'Test');
    $this->getSession()->getPage()->fillField('Confirm password', 'Test');
    $this->getSession()->getPage()->pressButton('Create new account');

    $this->drupalGet('/admin/reports/dblog');
    $this->assertSession()->pageTextContains('Human insert operation occured on user 3');

    $this->drupalGet('/user/3/edit');
    $this->getSession()->getPage()->fillField('Username', 'Test 2');
    $this->getSession()->getPage()->pressButton('Save');

    $this->drupalGet('/admin/reports/dblog');
    $this->assertSession()->pageTextContains('Human update operation occured on user 3');

    $this->drupalGet('/user/3/edit');
    $this->getSession()->getPage()->pressButton('Cancel account');
    $this->getSession()->getPage()->selectFieldOption('user_cancel_method', 'user_cancel_delete');
    $this->submitForm([], 'Cancel account');

    $this->drupalGet('/admin/reports/dblog');
    $this->assertSession()->pageTextContains('Human delete operation occured on user 3');
  }

}
