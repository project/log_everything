<?php

namespace Drupal\Tests\log_everything\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test config CRUD operation will generate logs.
 *
 * @group log_everything
 */
class ConfigEntityLogTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dblog', 'log_everything', 'user'];

  /**
   * A regular user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->webUser = $this->drupalCreateUser([], NULL, TRUE);
  }

  /**
   * Test that a config CRUD will register a log operation.
   */
  public function testConfigEntityLog() {
    $this->drupalLogin($this->webUser);

    $this->drupalGet('/admin/people/roles/add');
    $this->getSession()->getPage()->fillField('Role name', 'Test');
    $this->getSession()->getPage()->fillField('Machine-readable name', 'test');
    $this->getSession()->getPage()->pressButton('Save');

    $this->drupalGet('/admin/reports/dblog');
    $this->assertSession()->pageTextContains('Human insert operation occured on user_role test');

    $this->drupalGet('/admin/people/roles/manage/test');
    $this->getSession()->getPage()->fillField('Role name', 'Test 2');
    $this->getSession()->getPage()->pressButton('Save');

    $this->drupalGet('/admin/reports/dblog');
    $this->assertSession()->pageTextContains('Human update operation occured on user_role test');

    $this->drupalGet('/admin/people/roles/manage/test');
    $this->getSession()->getPage()->clickLink('Delete');
    $this->getSession()->getPage()->pressButton('Delete');

    $this->drupalGet('/admin/reports/dblog');
    $this->assertSession()->pageTextContains('Human delete operation occured on user_role test');
  }

}
