<?php

namespace Drupal\log_everything\Service;

use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The Log Everything Log Manager service.
 */
class LogManager {

  use StringTranslationTrait;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a Log Everything Log Manager instance.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Log an operation on a content entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the operation occured on.
   * @param string $operation
   *   The operation.
   */
  public function logContentEntityOperation(EntityInterface $entity, string $operation) {
    $this->loggerFactory->get('log_everything')
      ->log('info', $this->t('@interface @operation operation occured on @type @identifier', [
        '@interface' => PHP_SAPI === 'cli' ? 'Command line' : 'Human',
        '@operation' => $operation,
        '@type' => $entity->getEntityTypeId(),
        '@identifier' => $entity->id(),
      ]));
  }

  /**
   * Log an operation on a config entity.
   *
   * @param \Drupal\Core\Config\Config $entity
   *   The entity the operation occured on.
   * @param string $operation
   *   The operation.
   */
  public function logConfigEntityOperation(Config $entity, string $operation) {
    $this->loggerFactory->get('log_everything')
      ->log('info', $this->t('@interface @operation operation occured on "@identifier" config entity', [
        '@interface' => PHP_SAPI === 'cli' ? 'Command line' : 'Human',
        '@operation' => $operation,
        '@identifier' => $entity->getName(),
      ]));
  }

}
