<?php

namespace Drupal\log_everything\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\log_everything\Service\LogManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber on configuration crud operation.
 */
class ConfigCrudSubscriber implements EventSubscriberInterface {

  /**
   * The Log Everything Log Manager service.
   *
   * @var \Drupal\log_everything\Service\LogManager
   */
  protected $logManager;

  /**
   * Constructs a Log Everything Config Crud Subscriber instance.
   *
   * @param \Drupal\log_everything\Service\LogManager $log_manager
   *   The Log Everything Log Manager service.
   */
  public function __construct(LogManager $log_manager) {
    $this->logManager = $log_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE => 'onConfigSave',
      ConfigEvents::DELETE => 'onConfigDelete',
    ];
  }

  /**
   * React to a config object being saved.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();

    $operation = $config->isNew() ? 'insert' : 'update';
    $this->logManager->logConfigEntityOperation($config, $operation);
  }

  /**
   * React to a config object being deleted.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function onConfigDelete(ConfigCrudEvent $event) {
    $this->logManager->logConfigEntityOperation($event->getConfig(), 'delete');
  }

}
